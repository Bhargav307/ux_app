FROM python:3.8-slim-buster

WORKDIR /UX_app

COPY . .

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]

CMD ["app.py"]
