from flask import Flask, jsonify, json, render_template, request
import requests

app1 = Flask(__name__)


@app1.route('/')
def home():
	return render_template('home.html')



@app1.route('/output', methods=['POST'])
def output():
	if request.method == 'POST':
		# if request.form['result'] == 'Name':
		value = request.form['result']
		requestt = requests.get('http://127.0.0.1:5001/{}'.format(value)).json()
		keys = requestt[0].keys()
		return render_template('tablee.html', data=requestt, keys=keys)


if __name__ == '__main__':
	app1.run(debug=True)
